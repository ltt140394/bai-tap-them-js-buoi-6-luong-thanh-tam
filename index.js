function inSoNguyenTo() {
    var numberValue = document.getElementById("input-value").value * 1;
    var contentHTML = "";

    if (numberValue < 2) {
        document.getElementById("showKetQua").style.display = "block";
        document.getElementById("showKetQua").innerHTML = `Dưới ${numberValue} không có số nguyên tố`;
    } else {
        for (i = 2; i <= numberValue; i++) {           
            var flag = 0;
            for (j = 2; j < i; j++) {  
/*
Số nguyên tố chỉ chia hết cho 1 và chính nó, 2 trường hợp này đã loại trừ thông qua giá trị khởi tạo j và điều kiện lặp,
do đó khi tìm thấy chia hết cho 1 số khác nữa thì số này không phải là số nguyên tố, thay đổi giá trị biến flag để check
số nguyên tố và thoát vòng lặp.
*/                                
                if (i % j == 0) {
                    flag = 1;
                    break;                    
                }
            }
            if (flag == 0) {
                var content = i + " ";
                contentHTML += content;
            }
        }
        document.getElementById("showKetQua").style.display = "block";
        document.getElementById("showKetQua").innerHTML = contentHTML;
    }
}